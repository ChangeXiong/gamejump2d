using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class ScoreScripts : MonoBehaviour
{
    private Text coinTextScore;
    private AudioSource audioManager;
    private int scoreCount;

    void Awake()
    {
        audioManager = GetComponent<AudioSource>();
    }


    void Start()
    {
        coinTextScore = GameObject.Find("CoinsText").GetComponent<Text>();
    }

  
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == MyTags.COIN_TAG)
        {
            target.gameObject.SetActive(false);
            scoreCount++;
            coinTextScore.text = "x" + scoreCount.ToString();
            audioManager.Play();
        }
    }
}
