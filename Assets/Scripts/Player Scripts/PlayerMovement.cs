﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 5f;
    private Rigidbody2D rbBody;
    private Animator anim;

    public Transform groundCheckPosition;
    public LayerMask groundLayer;


    // Create Animation Jump
    private bool isGrounded;
    private bool jumped;

    private float jumpPower = 10.5f;
    //
    void Awake()
    {
        rbBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        
    }


    void Update()
    {
        CheckIfGrounded();
        PlayerJump();
    }
    void FixedUpdate()
    {
        PlayerWalk();
    }
    void PlayerWalk()
    {
        float Walk = Input.GetAxisRaw("Horizontal");
        if (Walk > 0)
        {
            rbBody.velocity = new Vector2(Speed, rbBody.velocity.y);// Make Animation going to;
            ChangeDirection(1);
        }else if(Walk < 0)
        {
            rbBody.velocity = new Vector2(-Speed, rbBody.velocity.y); // Make Animation Brack
            ChangeDirection(-1);
        }
        else
        {
            rbBody.velocity = new Vector2(0f, rbBody.velocity.y); // Make Animation Stop;
        }
        anim.SetInteger("Speed", Mathf.Abs((int)rbBody.velocity.x)); // get come to Animator and Make Animation Walk
    }
    void ChangeDirection(int direction) // function ใข้ในกานล้ๅน
    {
        Vector3 tempScale = transform.localScale;
        tempScale.x = direction;
        transform.localScale = tempScale;
    }
    void CheckIfGrounded()
    {
        isGrounded = Physics2D.Raycast(groundCheckPosition.position, Vector2.down, 0.1f, groundLayer);
        if (isGrounded)
        {
            if (jumped)
            {
                jumped = false;
                anim.SetBool("Jump", false);
            }
        }
    }
    void PlayerJump()
    {
        if (isGrounded)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                jumped = true;
                rbBody.velocity = new Vector2(rbBody.velocity.x, jumpPower);
                anim.SetBool("Jump", true);
            }
        }
    }

}
