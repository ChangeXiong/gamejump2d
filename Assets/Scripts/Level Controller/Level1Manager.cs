using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1Manager : MonoBehaviour
{
    private AudioSource soundFX;
    [SerializeField] private string nextLevelName;

    private void Awake()
    {
        soundFX = GetComponent<AudioSource>();
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(nextLevelName);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            NextLevel();
            soundFX.Play();

        }        
    }
}
