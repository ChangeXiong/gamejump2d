using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleBackground : MonoBehaviour
{

    void Start()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        transform.localScale = new Vector3(1, 1, 1);
        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenH = Camera.main.orthographicSize * 2f;
        float worldScreenW = worldScreenH / Screen.height * Screen.width;

        Vector3 tempScale = transform.localScale;
        tempScale.x = worldScreenW / width + 0.1f;
        tempScale.y = worldScreenH / height + 0.1f;

        transform.localScale = tempScale;
    }

  
    void Update()
    {
        
    }
}
