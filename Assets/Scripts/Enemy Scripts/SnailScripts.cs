using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SnailScripts : MonoBehaviour
{
    public float moveSpeed = 1f;
    private Rigidbody2D Snailbody;
    private Animator anim;

    public LayerMask playerLayer;

    private bool moveLeft;

    // Create Stunned
    private bool canMove;
    private bool stunned;

    public Transform left_Collision,rigth_Collision,top_Collision, down_Collision;
    private Vector3 left_Collision_Position, rigth_Collision_Position;

    void Awake()
    {
        Snailbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        left_Collision_Position = left_Collision.position;
        rigth_Collision_Position = rigth_Collision.position;
    }
    void Start()
    {
        moveLeft = true;
        canMove = true;
    }

    void Update()
    {
        if (canMove)
        {
            if (moveLeft)
            {
                Snailbody.velocity = new Vector2(-moveSpeed, Snailbody.velocity.y);
            }
            else
            {
                Snailbody.velocity = new Vector2(moveSpeed, Snailbody.velocity.y);
            }
        }
        CheckCollision();
    }
    void CheckCollision()
    {
        RaycastHit2D letfHit = Physics2D.Raycast (left_Collision.position, Vector2.left,0.1f, playerLayer);
        RaycastHit2D rigthHit = Physics2D.Raycast(rigth_Collision.position, Vector2.right, 0.1f, playerLayer);

        Collider2D topHit = Physics2D.OverlapCircle(top_Collision.position, 0.2f, playerLayer);
        if(topHit != null)
        {
            if(topHit.gameObject.tag == MyTags.PLAYER_TAG)
            {
                if (!stunned)
                {
                    topHit.gameObject.GetComponent<Rigidbody2D>().velocity =
                        new Vector2(topHit.gameObject.GetComponent<Rigidbody2D>().velocity.x, 7f);

                    canMove = false;
                    Snailbody.velocity = new Vector2(0, 0);
                    anim.Play("Stunned");
                    stunned = true;
                    // Beetle code here
                    if(tag == MyTags.BEETLE_TAG)
                    {
                        anim.Play("Stunned");
                        StartCoroutine(Dead(0.5f));
                    }
                }
            }
        }
        if (letfHit)
        {
            if (letfHit.collider.gameObject.tag == MyTags.PLAYER_TAG)
            {
                if (!stunned)
                {
                    // Apply Game To Player

                    rigthHit.collider.gameObject.GetComponent<PlayerDamage>().DealDamage();
                }
                else
                {
                    if(tag != MyTags.BEETLE_TAG)
                    {
                        Snailbody.velocity = new Vector2(15f, Snailbody.velocity.y);
                        StartCoroutine(Dead(3f));
                    }
                }
            }
        }
        if (rigthHit)
        {
            if(rigthHit.collider.gameObject.tag == MyTags.PLAYER_TAG)
            {
                if (!stunned)
                {
                    // Apply Game to player
                    rigthHit.collider.gameObject.GetComponent<PlayerDamage>().DealDamage();
                }
                else
                {
                    if (tag != MyTags.BEETLE_TAG)
                    {
                        Snailbody.velocity = new Vector2(-15f, Snailbody.velocity.y);
                        StartCoroutine(Dead(3f));
                    }
                }
            }
        }
        {
            
        }
        if (!Physics2D.Raycast(down_Collision.position, Vector2.down, 0.1f))
        {
            ChangeDirection();
        }
    }
    void ChangeDirection()
    {
        moveLeft = !moveLeft;
        Vector3 tempScale = transform.localScale;
        if (moveLeft)
        {
            tempScale.x = Mathf.Abs(tempScale.x);

            left_Collision.position = left_Collision_Position;
            rigth_Collision.position = rigth_Collision_Position;

        }
        else
        {
            tempScale.x = -Mathf.Abs(tempScale.x);

            left_Collision.position = rigth_Collision_Position;
            rigth_Collision.position = left_Collision_Position;
        }
        transform.localScale = tempScale;
    }
    IEnumerator Dead(float timer)
    {
        yield return new WaitForSeconds(timer);
        gameObject.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == MyTags.BULLET_TAG)
        {
            if(tag == MyTags.BEETLE_TAG)
            {
                anim.Play("Stunned");
                canMove = false;
                Snailbody.velocity = new Vector2(0, 0);
                StartCoroutine(Dead(0.4f));
            }
            if(tag == MyTags.SNAIL_TAG)
            {
                if (!stunned)
                {
                    anim.Play("Stunned");
                    stunned = true;
                    canMove = false;
                    Snailbody.velocity = new Vector2(0, 0);
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
    /*    void OnCollisionEnter2D(Collision2D target)
        {
            if(target.gameObject.tag == "Player")
            {
                anim.Play("Stunned");
            }
        }*/
}
