using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaremaFollow : MonoBehaviour
{
    public float resetSpeed = 0.5f;
    public float cameraSpeed = 0.3f;

    public Bounds cameraBounds;
    private Transform target;
    private float offsetZ;
    private Vector3 lastTargetPosition;
    private Vector3 currentVelocity;

    private bool followPlayer;

    void Awake()
    {
        BoxCollider2D myCol = GetComponent<BoxCollider2D>();
        myCol.size = new Vector2(Camera.main.aspect * 2f * Camera.main.orthographicSize, 15f);
        cameraBounds = myCol.bounds;
    }
    // Start is {called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag(MyTags.PLAYER_TAG).transform;
        lastTargetPosition = target.position;
        offsetZ = (transform.position - target.position).z;
        followPlayer = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (followPlayer)
        {
            Vector3 AheadTarget = target.position + Vector3.forward * offsetZ;
            if(AheadTarget.x >= transform.position.x)
            {
                Vector3 newCameraPostion = Vector3.SmoothDamp(transform.position, AheadTarget,
                    ref currentVelocity, cameraSpeed);
                transform.position = new Vector3(newCameraPostion.x, transform.position.y, 
                    newCameraPostion.z);
                lastTargetPosition = transform.position;
            }
        }
    }
}
